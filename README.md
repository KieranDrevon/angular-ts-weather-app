
# WeatherApp

## Description
This application allows users to input a city name and receive weather information based in that current location. The background image will change based off of the current temperature, displaying either a warm, or cold image. This was made using Angular, TypeScript, and includes usage of the Open Weather Map API. 

## Overview
I created this small project over a weekend as a way to familiarize myself with the fundamentals of Angular and TypeScript. I went through the main Angular tutorials and spent a lot of time reading through the documentation, and various other articles to be able to put this together. I am very happy with how this project has turned out, and all of the new things I learned from it. I am very eager to continue my Angular and TypeScript journey, as I know there is still so much more to learn.


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

