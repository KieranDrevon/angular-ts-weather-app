import { Component, OnInit } from '@angular/core';
import { WeatherInfo } from './models/weather.model';
import { WeatherService } from './services/weather.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(
    private weatherService: WeatherService,
    ) {}
    
    
    weatherInfo?: WeatherInfo;
    city: string = 'San Diego'
    icon: string = ''
    currentDate = new Date()
    currentTemp = 0

  ngOnInit(): void {
    this.getWeatherInfo(this.city)
    this.city = ''
  }

  onSubmit() {
    this.getWeatherInfo(this.city);
    this.city = '';
  }

  getWeatherInfo(city: string) {
    this.weatherService.getWeatherInfo(city)
      .subscribe({
        next: (res) => {
          this.weatherInfo = res
          this.icon = res.weather[0].icon
          this.currentTemp = res.main.temp
          console.log(this.weatherInfo)
        }
      })
  }
}
