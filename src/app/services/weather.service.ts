import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { WeatherInfo } from '../models/weather.model';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  constructor(
    private http: HttpClient
  ) { }

  getWeatherInfo(city: string): Observable<WeatherInfo> {
    return this.http.get<WeatherInfo>(environment.baseUrl, {
      params: new HttpParams()
      .set('q', city)
      .set('units', 'imperial')
      .set('appid', environment.apiKey)
    })
  }
}
